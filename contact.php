<ul class="nobul" style="margin-bottom: 2em">
    <li><a href="mailto:amy[at]rhiaro.co.uk"><i class="icon-envelope"></i> amy at rhiaro.co.uk</a></li>
    <li><a href="http://bitbucket.org/rhiaro"><i class="icon-hdd"></i> Code (Bitbucket)</a></li>
    <li><a href="http://twitter.com/rhiaro"><i class="icon-twitter"></i> @rhiaro</a></li>
    <li><a href="http://www.linkedin.com/profile/view?id=80896287"><i class="icon-linkedin-sign"></i> LinkedIn</a></li>
    <li><a href="http://last.fm/user/theringleader"><i class="icon-headphones"></i> Judge me (last.fm)</a></li>
</ul>