<?
require_once("pages.php");
$k = "doing";
$l = "current-status";

$request = str_replace("/rhiaro", "", $_SERVER['REQUEST_URI']); // (fix for localhost, shh)
$params = explode("/", $request);  
if(isset($params[1]) && $params[1] != "" && $params[1] != "index.php") $k = $params[1];
if(isset($params[2]) && $params[2] != "") $l = $params[2];
elseif($k == "did") $l = "pst-events";
elseif($k == "todo") $l = "ftr-events";
elseif($k == "doing") $l = "current-status";
else $l = "404";
/* When filtering by event participation */
if($l == "speaker" || $l == "organiser" || $l == "attendee"){
    $subl = $l;
    $l = "pst-events";
}

if(isset($subl)){
    $feedl = $subl;
}else{
    $feedl = $l;
}

// Start counting time for the page load
$starttime = explode(' ', microtime());
$starttime = $starttime[1] + $starttime[0];

// Include SimplePie
// Located in the parent directory
include_once('lib/simplepie/autoloader.php');
include_once('lib/simplepie/idn/idna_convert.class.php');

// Create a new instance of the SimplePie object
$feed = new SimplePie();
// Remember to change max-results if I ever think I'm going to have a label with more than 200 posts... or find a better way of doing this.
$url = "http://rhiaro.blogspot.com/feeds/posts/default/-/".$feedl."?max-results=200";
$feed->set_feed_url($url);

$success = $feed->init();
$feed->handle_content_type();
$no = $max = $feed->get_item_quantity();

if(!array_key_exists($l, $pages)){ 
    if($no == 0) $l = "404";
}

if($l == "current-status"){
    $latestfeed = new SimplePie();
    $latesturl = "http://rhiaro.blogspot.com/feeds/posts/default/?max-results=1";
    $latestfeed->set_feed_url($latesturl);
    $latestsuccess = $latestfeed->init();
    $latestfeed->handle_content_type();
    foreach($latestfeed->get_items() as $item){
        $latestlink = $item->get_permalink();
        $latesttitle = $item->get_title();
        $latestdate = $item->get_date();
    }
}

?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <title>What Amy Does.</title>
        <meta name="description" content=""/>
        <meta name="viewport" content="width=device-width"/>
        <!--<base href="//localhost/rhiaro/index.php" />-->
        <base href="http://rhiaro.co.uk" />

        <link href='http://fonts.googleapis.com/css?family=Londrina+Sketch|Bitter:400,700,400italic' rel='stylesheet' type='text/css'/>
        <link rel="stylesheet" href="css/font-awesome.min.css"/>

        <link rel="stylesheet" href="css/normalize.min.css"/>
        <link rel="stylesheet" href="css/main.css"/>

        <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body class="color3-texture dark">
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <div class="w1of1 tall clearfix" id="top">
                <header class="w1of6 color1-texture tall clearfix">
                    <hgroup><a href="">
                        <h1>Amy Guy</h1>
                        <h2><span class="h2-1">A kanban</span> <span class="h2-2">of my life</span></h2>
                    </a></hgroup>

                    <nav>
                        <ul class="unpad">
                            <li>
                                <a class="color2border-hov hborder<?=($k=="did")?" color2-texture":""?>" href="did/pst-events#did">
                                    <i class="icon-check"></i> Did &nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-double-angle-right"></i>
                                </a>
                            </li>
                            <li>
                                <a class="color2border-hov hborder<?=($k=="doing") ? " color2-texture":""?>" href="doing/current-status#doing">
                                    <i class="icon-tasks"></i> Doing <i class="icon-double-angle-right"></i>
                                </a>
                            </li>
                            <li>
                                <a class="color2border-hov hborder<?=($k=="todo")?" color2-texture":""?>" href="todo/ftr-events#todo">
                                    <i class="icon-spinner"></i> To do &nbsp;<i class="icon-double-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </header>
                <?if($l!="home"):?>
                    <p class="btn-back mob color1-texture w1of1 clearfix" id="content">
                        <a class="color2border-hov hborder" href="index.php<?=isset($k)?$k:""?>#<?=isset($k)?$k:""?>">
                            <i class="icon-double-angle-left"></i>
                        </a>
                    </p>
                <?endif?>
                <div class="w1of2 color3-texture align-left pos3"><div class="inner padmore">
                    <?if ($feed->error()):?>
                        <p class="fail">Uh oh, something went wrong: <?=htmlspecialchars($feed->error())?></p>
                    <?elseif($success):?>
                        <?=(isset($pages[$l]))?$pages[$l]:""?>
                        <?if($l == "current-status"):?>
                            <? include 'contact.php'; ?>
                            <h3>And as of <?=$latestdate?></h3>
                            <h4>Things you might find me doing include</h4>
                            <?if(!$latestfeed->error()):?>
                                <p><strong>Blogging: YES</strong></p>
                                <p>Latest post: <a href="<?=$latestlink?>"><?=$latesttitle?></a></p>
                            <?endif?>
                        <?endif?>
                        <? //var_dump($feed->get_items()); ?>
                        <?if($no > 0 && $l != "current-status"):?>
                            <p>There <?=($no > 1)?"are":"is"?> <?=$no?> post<?=($no > 1)?"s":""?> about this!</p>
                        <?endif?>
                        <?foreach($feed->get_items() as $item):?>
                            <?
                            $idparts = explode('-',$item->get_id());
                            $pid = $idparts[(count($idparts)-1)];
                            ?>
                            <?if($l != "current-status"):?>
                                <h3><?=$item->get_date('j M Y, g:i a')?></h3>
                            <?endif?>
                            <h4>
                                <?if($l != "current-status"):?>
                                    <a href="<?=$item->get_permalink()?>"><?=$item->get_title()?></a>
                                <?endif?>
                            </h4>
                            <?=$item->get_content()?>
                            <?if($l != "current-status"):?>
                                <p class="right wee lighter"><span class="color1">Comments:</span> <a class="inner color1-texture" href="http://www.blogger.com/comment.g?blogID=18505529&postID=<?=$pid?>">leave</a> / <a class="inner color1-texture" href="<?=$item->get_permalink()?>#comments">view</a></p>
                            <?endif?>
                        <?endforeach?>
                    <?endif?>
                </div></div>
            </div>
            <div class="w1of3 color2-texture tall pos2"><div class="inner menu-tweak">
                <?if($k=="did" || $k=="home"):?>
                    <p class="menu-icon"><i class="icon-check color2-texture" id="did"></i></p>
                    <ul class="nav2 color2-texture">
                        <li>
                            <a href="did/pst-events#content" class="color3border-hov hborder<?=($l=="pst-events")?" color3-texture":""?>">Events (organised / presented at / attended)</a>
                        </li>
                        <li>
                            <a href="did/icp#content" class="color3border-hov hborder<?=($l=="icp")?" color3-texture":""?>">MSc by Research Interdisciplinary Creative Practices</a>
                        </li>
                        <li>
                            <a href="did/google-internship#content" class="color3border-hov hborder<?=($l=="google-internship")?" color3-texture":""?>">Google internship</a>
                        </li>
                        <li>
                            <a href="did/lincoln%20su#content" class="color3border-hov hborder<?=($l=="lincoln su")?" color3-texture":""?>">Lincoln Students' Union</a>
                        </li>
                        <li>
                            <a href="did/bsc#content" class="color3border-hov hborder<?=($l=="bsc")?" color3-texture":""?>">Undergraduate and earlier</a>
                        </li>
                        <li>
                            <a href="did/travel#content" class="color3border-hov hborder<?=($l=="travel")?" color3-texture":""?>">Travel</a>
                        </li>
                        <li>
                            <a href="did/portfolio#content" class="color3border-hov hborder<?=($l=="portfolio")?" color3-texture":""?>">Portfolio / projects</a>
                        </li>
                        <li>
                            <a class='color3border-hov hborder<?=($l=="misc")?" color3-texture":""?>' href='did/misc#content'>Other</a>
                        </li>
                        <li class="mob color1-texture align-center">
                            <a href="#top"><i class="icon-chevron-up"></i></a>
                        </li>
                    </ul>
                <?endif?>
                <?if($k=="doing" || $k=="home"):?>
                    <p class="menu-icon"><i class="icon-tasks color2-texture" id="doing"></i></p>
                    <ul class='nav2 color2-texture'>
                        <li>
                            <a class='color3border-hov hborder<?=($l=="current-status")?" color3-texture":""?>' href='doing/current-status#content'>Current status</a>
                        </li>
                        <li>
                            <a class='color3border-hov hborder<?=($l=="phd")?" color3-texture":""?>' href='doing/phd#content'>PhD (aka foreveracademia)</a>
                        </li>
                        <li>
                            <a class='color3border-hov hborder<?=($l=="hacking")?" color3-texture":""?>' href='doing/hacking#content'>Hacking / Learning</a>
                        </li>
                        <li>
                            <a class='color3border-hov hborder<?=($l=="geolit")?" color3-texture":""?>' href='doing/geolit#content'>GeoLit</a>
                        </li>
                        <li>
                            <a class='color3border-hov hborder<?=($l=="writing")?" color3-texture":""?>' href='doing/writing#content'>Writing</a>
                        </li>
                        <li>
                            <a class='color3border-hov hborder<?=($l=="tutoring")?" color3-texture":""?>' href='doing/tutoring#content'>Tutoring</a>
                        </li>
                        <li>
                            <a class='color3border-hov hborder<?=($l=="freegle")?" color3-texture":""?>' href='doing/freegle#content'>Freegle</a>
                        </li>
                        <li>
                            <a class='color3border-hov hborder<?=($l=="mbaking")?" color3-texture":""?>' href='doing/mbaking#content'>{M,B}aking</a>
                        </li>
                        <li>
                            <a class='color3border-hov hborder<?=($l=="parrot")?" color3-texture":""?>' href='doing/parrot#content'>Parrot behaviour</a>
                        </li>
                        <li class="mob color1-texture align-center">
                            <a href="#top"><i class="icon-chevron-up"></i></a>
                        </li>
                    </ul>
                <?endif?>
                <?if($k=="todo" || $k=="home"):?>
                    <p class="menu-icon"><i class="icon-spinner color2-texture" id="todo"></i></p>
                    <ul class="nav2 color2-texture">
                        <li>
                            <a href="todo/ftr-events#content" class="color3border-hov hborder<?=($l=="ftr-events")?" color3-texture":""?>">Events</a>
                        </li>
                        <li>
                            <a href="todo/projects#content" class="color3border-hov hborder<?=($l=="projects")?" color3-texture":""?>">Projects</a>
                        </li>
                        <li>
                            <a href="todo/to-learn#content" class="color3border-hov hborder<?=($l=="to-learn")?" color3-texture":""?>">Things to learn</a>
                        </li>
                        <li>
                            <a href="todo/ftr-travel#content" class="color3border-hov hborder<?=($l=="ftr-travel")?" color3-texture":""?>">Travel</a>
                        </li>
                        <li class="mob color1-texture align-center">
                            <a href="#top"><i class="icon-chevron-up"></i></a>
                        </li>
                    </ul>
                <?endif?>
            </div></div>


        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.3.min.js"><\/script>')</script>

        <script>
            var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    </body>
</html>
